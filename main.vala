using Gtk;
using Vte;
using Posix;

public int pid=0;
public string sterm_name;
public string[] arguments;
public Window App;

public class scrolled : ScrolledWindow{
	public Widget vte=null;
	public override void add (Widget w){
		vte=w;
		base.add(vte);
	}
}

int main(string[] args){
	wsl_block();
	arguments=args;
	pid=(int)Posix.getpid();
	sterm_name="sterm - "+(pid).to_string();
	if("--help" in args || "-h" in args){
		Posix.stdout.printf("\n");
		Posix.stdout.printf("\033[35;1mUsage:\033[;0m "+args[0]+" [--daemon/--no-tray/--no-header] [--bare]\n");
		Posix.stdout.printf("\033[34;1m--daemon    \033[;0m:\t Daemon mode. create trayicon but not open window.\n");
		Posix.stdout.printf("\033[34;1m--no-tray   \033[;0m:\t Open a window but not create tray icon.\n");
		Posix.stdout.printf("\033[34;1m--bare      \033[;0m:\t Pure VTE mode. Does'nt have bottom bar.\n");
		Posix.stdout.printf("\033[34;1m--no-header \033[;0m:\t No header. Classic layout.\n");
		Posix.stdout.printf("\033[34;1m--help      \033[;0m:\t Show this message\n");
		Posix.stdout.printf("\n");
		Posix.stdout.printf("\033[35;1mHotkeys:\033[;0m ctrl + shift + [a/n/r/<-/->/del/f11/v/c/+/-]\n");
		Posix.stdout.printf("\033[33;1m a          \033[;0m:\t Select all\n");
		Posix.stdout.printf("\033[33;1m n          \033[;0m:\t New tab\n");
		Posix.stdout.printf("\033[33;1m r          \033[;0m:\t Reset\n");
		Posix.stdout.printf("\033[33;1m <-         \033[;0m:\t Previous tab\n");
		Posix.stdout.printf("\033[33;1m ->         \033[;0m:\t Next tab\n");
		Posix.stdout.printf("\033[33;1m del        \033[;0m:\t Delete tab\n");
		Posix.stdout.printf("\033[33;1m f11        \033[;0m:\t Fullscreen/Leave Fullscreen\n");
		Posix.stdout.printf("\033[33;1m v          \033[;0m:\t Paste\n");
		Posix.stdout.printf("\033[33;1m c          \033[;0m:\t Copy\n");
		Posix.stdout.printf("\033[33;1m -          \033[;0m:\t Zoom out\n");
		Posix.stdout.printf("\033[33;1m +          \033[;0m:\t Zoom in\n");
		Posix.stdout.printf("\n");
		return 0;
	}
	Gtk.init(ref args);
	if(("--no-tray" in args)==false){
		App = new AppStatusIcon(args,sterm_name);
		if(("--daemon" in args)==false){
			((AppStatusIcon)App).new_sterm();
		}
	}else{
		if(("--daemon" in args)==false){
			sterm s = new sterm();
			var w=s.sterm_init(args,sterm_name);
			w.destroy.connect(Gtk.main_quit);
		}
	}
	Gtk.main();
	return 0;
}

public class sterm : Gtk.Window{
	public Notebook n;
	public Window w;
	public Gtk.Menu Wmenu;
	public GLib.Settings settings;
	public bool isfullscreen=false;
	
	public Vte.Terminal bareVTE;
	
	public Gtk.Window sterm_init(string[] args,string WindowTitle){
		w = new Gtk.Window();
		settings = new GLib.Settings("org.sulin.sterm");
		w.icon = new Gdk.Pixbuf.from_file ("/usr/share/icons/sterm.svg");
		if("--bare" in args){
			bareVTE=getVTE();
			w.add(bareVTE);
		}else{
			var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
			var box2 = new Gtk.HeaderBar ();
			n=getNotebook();
			w.add(box);
			box2.pack_start(addNew());
			box2.pack_start(fullscreen());
			box2.pack_start(reset());
			
			box2.pack_end(paste());
			box2.pack_end(copy());
			box2.pack_end(bgr());
			box2.pack_end(fgr());
			
			box.pack_start (n);
			if("--no-header" in args){
				box.pack_start(box2,false,false,0);
			}else{
				box2.set_show_close_button(true);
				w.set_titlebar (box2);
			}
			w.set_title(WindowTitle);
			
		}
		menu_init(arguments);
		w.button_press_event.connect(display_menu);
		w.key_press_event.connect(hotkeys);
		w.show_all();
		w.set_opacity(0.95);
		return w;
	}
	public Gtk.ImageMenuItem menufull;
	public void menu_init(string[] args){
		Wmenu = new Gtk.Menu();
		if(("--bare" in args)== false){
			var menuAdd = new ImageMenuItem.from_stock(Stock.ADD,null);
			menuAdd.set_label("New Tab");
			menuAdd.activate.connect(()=>{
				create_notebook_child_label ("sterm");
			});
			Wmenu.append(menuAdd);
		}
		
		menufull = new ImageMenuItem.from_stock(Stock.FULLSCREEN,null);
		menufull.activate.connect(()=>{
			if(!isfullscreen){
				w.fullscreen();
			}else{
				w.unfullscreen();
			}
			isfullscreen=!isfullscreen;
		});
		Wmenu.append(menufull);
		
		var menucopy = new ImageMenuItem.from_stock(Stock.COPY,null);
		menucopy.activate.connect(()=>{
			getCurrentVTE(arguments).copy_clipboard();
		});
		Wmenu.append(menucopy);
		
		var menupaste = new ImageMenuItem.from_stock(Stock.PASTE,null);
		menupaste.activate.connect(()=>{
			getCurrentVTE(arguments).paste_primary();
		});
		Wmenu.append(menupaste);
		
		var menuSelectAll = new ImageMenuItem.from_stock(Stock.SELECT_ALL,null);
		menuSelectAll.activate.connect(()=>{
			getCurrentVTE(arguments).select_all();
		});
		Wmenu.append(menuSelectAll);
		
		var menuzoom100 = new ImageMenuItem.from_stock(Stock.ZOOM_100,null);
		menuzoom100.activate.connect(()=>{
			getCurrentVTE(arguments).set_font_scale(1);
		});
		Wmenu.append(menuzoom100);
		
		var menuzoomIN = new ImageMenuItem.from_stock(Stock.ZOOM_IN,null);
		menuzoomIN.activate.connect(()=>{
			getCurrentVTE(arguments).set_font_scale(getCurrentVTE(arguments).get_font_scale()+0.1);
		});
		Wmenu.append(menuzoomIN);
		
		var menuzoomOUT = new ImageMenuItem.from_stock(Stock.ZOOM_OUT,null);
		menuzoomOUT.activate.connect(()=>{
			getCurrentVTE(arguments).set_font_scale(getCurrentVTE(arguments).get_font_scale()-0.1);
		});
		Wmenu.append(menuzoomOUT);
		
		var menureset = new ImageMenuItem.from_stock(Stock.REFRESH,null);
		menureset.set_label("Reset");
		menureset.activate.connect(()=>{
			getCurrentVTE(arguments).reset(false,true);
			var command = GLib.Environment.get_variable ("SHELL");
		getCurrentVTE(arguments).spawn_sync (Vte.PtyFlags.DEFAULT,null,new string[] { command },null,0,null,null);
			
		});
		Wmenu.append(menureset);
		
		
		Wmenu.show_all();
	}
	
	private bool hotkeys(Gdk.EventKey event){
		if(event.state == (Gdk.ModifierType.SHIFT_MASK | Gdk.ModifierType.CONTROL_MASK)){
			int keycode=event.hardware_keycode;
			switch(keycode){
				case 54://ctrl shift v
					getCurrentVTE(arguments).paste_primary();
					break;
				case 55://ctrl shift c
					getCurrentVTE(arguments).copy_clipboard();
					break;
				case 95://ctrl shift f11
					if(!isfullscreen){
						w.fullscreen();
					}else{
						w.unfullscreen();
					}
					isfullscreen=!isfullscreen;
					break;
				case 41://ctrl shift a
					getCurrentVTE(arguments).select_all();
					break;
				case 119://ctrl shift del
					n.remove_page(n.get_current_page());
					break;
				case 114://ctrl shift ->
					n.next_page();
					break;
				case 113://ctrl shift <-
					n.prev_page();
					break;
				case 31://ctrl shift n
					create_notebook_child_label ("sterm");
					break;
				case 30://ctrl shift r
					getCurrentVTE(arguments).reset(false,true);
					var command = GLib.Environment.get_variable ("SHELL");
					getCurrentVTE(arguments).spawn_sync (Vte.PtyFlags.DEFAULT,null,new string[] { command },null,0,null,null);
					break;
				case 86://ctrl shift +
					getCurrentVTE(arguments).set_font_scale(getCurrentVTE(arguments).get_font_scale()+0.1);
					break;
				case 82://ctrl shift -
					getCurrentVTE(arguments).set_font_scale(getCurrentVTE(arguments).get_font_scale()-0.1);
					break;
				default:
					return false;
			}
			return true;
		}
		return false;
	}
	
	private bool display_menu(Gdk.EventButton event){
		if(event.button == 3){//sağ tık menüsü
			if(isfullscreen){
				menufull.set_image(new Gtk.Image.from_stock(Stock.LEAVE_FULLSCREEN, Gtk.IconSize.MENU));
				menufull.set_label("Leave Fullscreen");
			}else{
				menufull.set_image(new Gtk.Image.from_stock(Stock.FULLSCREEN, Gtk.IconSize.MENU));
				menufull.set_label("Fullscreen");
			}
			Wmenu.popup(null, null, null, event.button, event.time);
			return true;
		}
		return false;
	}
	public Vte.Terminal getCurrentVTE(string[] args){
		Vte.Terminal vte;
		if(("--bare" in args)== false){
			vte=(Vte.Terminal)((scrolled)n.get_nth_page(n.get_current_page())).vte;
		}else{
			vte=bareVTE;
		}
		return vte;
	}
	public Gtk.Notebook getNotebook(){
		n = new Gtk.Notebook();
		var t=new Gtk.Button();
		n.set_scrollable(true);
		n.set_show_tabs(false);
		create_notebook_child_label ("sterm");
		n.page_added.connect((w,num)=>{
			n.show_all();
			n.set_current_page((int)num);
			n.set_show_tabs(true);
		});
		n.show_all();
		n.set_current_page(1);
		n.page_removed.connect( (o, page, num) => {
			if(n.get_n_pages() == 1 ){
				n.set_show_tabs(false);
			}else if(n.get_n_pages() == 0 ){
				w.destroy();
			}
		});
		return n;
	}
	public Vte.Terminal getVTE(){
		var v = new Vte.Terminal();
		var command = GLib.Environment.get_variable ("SHELL");
	v.spawn_sync (Vte.PtyFlags.DEFAULT,null,new string[] { command },null,0,null,null);
		v.set_scrollback_lines(-1);
		v.set_color_background(color(settings.get_string("bgr")));
		v.set_color_foreground(color(settings.get_string("fgr")));
		return v;
	}
	
	public Gdk.RGBA color(string c){
		var rgba = Gdk.RGBA();
		rgba.parse(c);
		return rgba;
	}
	public string hex(Gdk.RGBA color){
		string s ="#%02x%02x%02x"
		.printf((uint)(255*color.red),
		(uint)(255*color.green),
		(uint)(255*color.blue));
		return s;
	}
	Widget create_notebook_child_label(string text){
		var vte=getVTE();
		var label = new Gtk.Label(text);
		var image = new Gtk.Image.from_stock(Gtk.Stock.DELETE, Gtk.IconSize.MENU);
		var button = new Gtk.Button();
		var scrollbar = new scrolled();
		
		button.relief = ReliefStyle.NONE;
		button.name = "sample-close-tab-button";
		button.clicked.connect(()=>{
			n.remove_page(n.page_num(scrollbar));
		});
		vte.window_title_changed.connect( (term) => {
			var TabTitle=vte.get_window_title();
			if(TabTitle.size()>23){
				TabTitle=TabTitle.substring (0, 20)+"...";
			}
			label.set_label(TabTitle);
		});
		vte.child_exited.connect(()=> {
			n.remove_page(n.page_num(scrollbar));
		});
		button.set_focus_on_click(false);
		button.add(image);
		var hbox = new HBox(false, 2);
		hbox.pack_start(label, false, false ,0);
		hbox.pack_start(button, false, false ,0);
		hbox.show_all();
		scrollbar.add(vte);
		scrollbar.set_min_content_width(640);//Altın oran
		scrollbar.set_min_content_height(400);//Altın oran
		n.append_page (scrollbar, hbox);
		n.set_tab_reorderable(scrollbar,true);
		return hbox;
	}
	
	public Gtk.Button addNew(){
		var b = new Gtk.Button();
		var image = new Gtk.Image.from_stock(Gtk.Stock.ADD, Gtk.IconSize.MENU);
		b.add(image);
		b.set_focus_on_click(false);
		b.relief = ReliefStyle.NONE;
		b.clicked.connect(()=>{
			create_notebook_child_label ("sterm");
		});
		return b;
	}
	public Gtk.Button reset(){
		var b = new Gtk.Button();
		var image = new Gtk.Image.from_stock(Gtk.Stock.REFRESH, Gtk.IconSize.MENU);
		b.add(image);
		b.set_focus_on_click(false);
		b.relief = ReliefStyle.NONE;
		b.clicked.connect(()=>{
			var vte=(Vte.Terminal)((scrolled)n.get_nth_page(n.get_current_page())).vte;
			(vte).reset(false,true);
			var command = GLib.Environment.get_variable ("SHELL");
		vte.spawn_sync (Vte.PtyFlags.DEFAULT,null,new string[] { command },null,0,null,null);
		});
		return b;
	}
	public Gtk.Button paste(){
		var b = new Gtk.Button();
		var image = new Gtk.Image.from_stock(Gtk.Stock.PASTE, Gtk.IconSize.MENU);
		b.add(image);
		b.set_focus_on_click(false);
		b.relief = ReliefStyle.NONE;
		b.clicked.connect(()=>{
			var vte=(Vte.Terminal)((scrolled)n.get_nth_page(n.get_current_page())).vte;
			(vte).paste_primary();
		});
		return b;
	}public Gtk.Button copy(){
		var b = new Gtk.Button();
		var image = new Gtk.Image.from_stock(Gtk.Stock.COPY, Gtk.IconSize.MENU);
		b.add(image);
		b.set_focus_on_click(false);
		b.relief = ReliefStyle.NONE;
		b.clicked.connect(()=>{
			var vte=(Vte.Terminal)((scrolled)n.get_nth_page(n.get_current_page())).vte;
			(vte).copy_clipboard();
		});
		return b;
	}public Gtk.Button fullscreen(){
		var b = new Gtk.Button();
		var image = new Gtk.Image.from_stock(Gtk.Stock.FULLSCREEN, Gtk.IconSize.MENU);
		b.add(image);
		b.set_focus_on_click(false);
		b.relief = ReliefStyle.NONE;
		b.clicked.connect(()=>{
			w.fullscreen();
			isfullscreen=true;
		});
		return b;
	}public Gtk.ColorButton bgr(){
		var blue = Gdk.RGBA ();
		blue.parse (settings.get_string("bgr"));
		var b= new Gtk.ColorButton.with_rgba (blue);
		b.color_set.connect ((widget)=>{
			var color = b.get_rgba ();
			for(var i=0;i<n.get_n_pages();i++){
				var vte=(Vte.Terminal)((scrolled)n.get_nth_page(i)).vte;
				(vte).set_color_background(color);
				settings.set_string("bgr",hex(color));
			}
		});
		return b;
	}public Gtk.ColorButton fgr(){
		var blue = Gdk.RGBA ();
		blue.parse (settings.get_string("fgr"));
		var b= new Gtk.ColorButton.with_rgba (blue);
		b.color_set.connect ((widget)=>{
			var color = b.get_rgba ();
			for(var i=0;i<n.get_n_pages();i++){
				var vte=(Vte.Terminal)((scrolled)n.get_nth_page(i)).vte;
				settings.set_string("fgr",hex(color));
				(vte).set_color_foreground(color);
			}
		});
		return b;
	}
}
int wsl_block(){
	  var file = File.new_for_path ("/proc/version");
	  var dis = new DataInputStream (file.read ());
	  string line=dis.read_line(null);
	  if("Microsoft" in line || "WSL" in line){
		    _exit(1);
	  }
	  return 0;
}
string string_random(int length = 8, string charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"){
	  string random = "";
	
	  for(int i=0;i<length;i++){
		    int random_index = Random.int_range(0,charset.length);
		    string ch = charset.get_char(charset.index_of_nth_char(random_index)).to_string();
		    random += ch;
	  }
	
	  return random;
}
