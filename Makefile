
DESTDIR=/usr
all: clean build

build:
	valac main.vala AppStatusIcon.vala --pkg gtk+-3.0 --pkg vte-2.91 --pkg gio-2.0 --pkg posix -o sterm
install:
	mkdir -p $(DESTDIR)/bin || true
	mkdir -p $(DESTDIR)/share/applications || true
	mkdir -p $(DESTDIR)/share/icons || true
	install sterm $(DESTDIR)/bin/sterm
	cp -prf gsettings.xml $(DESTDIR)/share/glib-2.0/schemas/org.sulin.sterm.gschema.xml
	glib-compile-schemas $(DESTDIR)/share/glib-2.0/schemas/
	install sterm.svg $(DESTDIR)/share/icons/sterm.svg
	install sterm.desktop $(DESTDIR)/share/applications/sterm.desktop
clean:
	rm -f sterm || true
