using Gtk;

class AppStatusIcon : Window {
	private StatusIcon trayicon;
	private Gtk.Menu menuSystem;
	private string[] args;
	private string WindowTitle;
	private int i=0;
	public AppStatusIcon(string[] a,string TooltipText) {
		/* Create tray icon */
		args=a;
		WindowTitle=TooltipText;
		trayicon = new StatusIcon.from_file ("/usr/share/icons/sterm.svg");;
		trayicon.set_tooltip_text (TooltipText);
		trayicon.set_visible(true);
		
		trayicon.activate.connect(new_sterm);
		
		create_menuSystem();
		trayicon.popup_menu.connect(menuSystem_popup);
	}
	
	/* Create menu for right button */
	public void create_menuSystem() {
		menuSystem = new Gtk.Menu();
		var menuNew = new ImageMenuItem.from_stock(Stock.NEW, null);
		menuNew.set_label("New Window");
		menuNew.activate.connect(new_sterm);
		menuSystem.append(menuNew);
		var menuQuit = new ImageMenuItem.from_stock(Stock.QUIT, null);
		menuQuit.activate.connect(Gtk.main_quit);
		menuSystem.append(menuQuit);
		menuSystem.show_all();
	}
	
	/* Show popup menu on right button */
	private void menuSystem_popup(uint button, uint time) {
		menuSystem.popup(null, null, null, button, time);
	}
	
	public void new_sterm() {
		sterm s = new sterm();
		Gtk.Window w=s.sterm_init(args,WindowTitle);
	}
}
